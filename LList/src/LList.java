import java.util.Arrays;

public class LList implements IList {

    private static class Node {
        int data;
        Node next;

        public Node() {
            this.data = 0;
            this.next = null;
        }

        public Node(int data) {
            this.data = data;
            this.next = null;
        }

        public Node(int data, Node next) {
            this.data = data;
            this.next = next;
        }

        public void setData(int data) {
            this.data = data;
        }

        public void setNext(Node next) {
            this.next = next;
        }

        @Override
        public String toString() {
            return "Node{data=" + data + ", next=" + next + "}";
        }
    }

    private int size;
    private Node head;

    public LList() {
        head = null;
        size = 0;
    }

    public LList(int[] ints) {
        addAll(ints);
    }


    private Node getNode(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }
        Node node = head;
        for (int i = 0; i < index; i++) {
            node = node.next;
        }
        return node;
    }

    @Override
    public void clear() {
        head = null;
        size = 0;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public int get(int index) {
        if (index < 0)
            throw new RuntimeException("Index is wrong");
        if (index == 0)
            return head.data;

        Node current = null;
        if (head != null) {
            current = head.next;
            for (int i = 0; i < index - 1; i++) {
                if (current.next == null)
                    throw new RuntimeException("Index is wrong");
                current = current.next;
            }
            return current.data;
        }
        throw new RuntimeException("LList is empty");
    }

    private void addAll(int[] array) {
        for (int j : array) {
            add(j);
        }
    }

    @Override
    public boolean add(int number) {
        if (head == null) {
            head = new Node(number);
        } else {
            Node node = head;
            while (node.next != null) {
                node = node.next;
            }
            node.next = new Node(number);
        }
        size++;
        return true;
    }

    @Override
    public boolean add(int index, int number) {
        Node temp = new Node(number);
        Node node = head;
        if (node != null) {
            for (int i = 0; i < index - 1 && node.next != null; i++) {
                node = node.next;
            }
        }
        assert node != null;
        temp.setNext(node.next);
        node.setNext(temp);
        size++;
        return true;
    }

    @Override
    public int remove(int number) {
        int temp = 0;
        Node node = head;
        while (node.data != number) {
            node = node.next;
            temp++;
        }
        removeByIndex(temp);
        return number;
    }

    @Override
    public int removeByIndex(int index) {
        if (index == 0) {
            head = head.next;
        } else {
            Node node = getNode(index - 1);
            node.next = node.next.next;
        }
        size--;
        return index;
    }

    @Override
    public boolean contains(int number) {
        Node node = head;
        if (node.data == number) {
            return true;
        }
        while (node.next != null) {
            node = node.next;
            if (node.data == number) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void print() {
        System.out.println(Arrays.toString(toArray()));
    }

    @Override
    public int[] toArray() {
        int[] array = new int[size];
        int i = 0;
        for (Node node = head; node != null; node = node.next) {
            array[i] = node.data;
            i++;
        }
        return array;
    }

    @Override
    public IList subList(int fromIndex, int toIndex) {
        if (fromIndex < 0 || toIndex >= size || fromIndex > toIndex) {
            throw new IndexOutOfBoundsException();
        }
        int i = 0;
        LList lList = new LList();
        for (Node node = head; node != null; node = node.next) {
            if (i >= fromIndex && i <= toIndex) {
                lList.add(node.data);
            }
            i++;
        }
        return lList;
    }

    @Override
    public boolean removeAll(int[] arr) {
        for (int j : arr) {
            remove(j);
        }
        return true;
    }

    @Override
    public boolean retainAll(int[] arr) {
        int[] currentArray = toArray();
        int temp = 0;
        for (int value : currentArray) {
            for (int i : arr) {
                if (value == i) {
                    temp++;
                }
            }
        }
        int[] result = new int[temp];
        int k = 0;
        for (int value : currentArray) {
            for (int i : arr) {
                if (value == i) {
                    result[k++] = value;
                }
            }
        }
        currentArray = result;
        clear();
        addAll(currentArray);
        return true;
    }

    @Override
    public String toString() {
        return "LList{head=" + head + "}";
    }
}
