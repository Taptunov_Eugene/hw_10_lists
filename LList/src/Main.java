public class Main {
    public static void main(String[] args) {
        LList lList = new LList();
        lList.add(1);
        lList.add(2);
        lList.add(3);
        lList.print();
        System.out.println("size=" + lList.size());

        lList.remove(1);
        lList.print();
        System.out.println("size=" + lList.size());

        lList.add(2, 2);
        lList.print();
        System.out.println("size=" + lList.size());

        lList.removeByIndex(1);
        lList.print();
        System.out.println("size=" + lList.size());

    }
}
