import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class LListTest {
    private LList lList;

    @BeforeEach
    public void setlList() {
        lList = new LList();
        lList.add(1);
        lList.add(2);
        lList.add(3);
    }

    @org.junit.jupiter.api.Test
    void clear() {
        //given

        //when
        lList.clear();

        //then
        assertEquals(0, lList.size());
    }

    @org.junit.jupiter.api.Test
    void size() {
        //given

        //when
        int actual = lList.size();

        //then
        assertEquals(3, actual);
    }

    @org.junit.jupiter.api.Test
    void get() {
        //given

        //when
        int actual = lList.get(1);

        //then
        assertEquals(2, actual);
        assertNotEquals(3, actual);
    }

    @org.junit.jupiter.api.Test
    void testAdd() {
        //given
        int number = 4;

        //when
        boolean actual = lList.add(number);

        //then
        assertTrue(actual);
    }

    @org.junit.jupiter.api.Test
    void testAddByIndex() {
        //given
        int index = 3, number = 5;

        //when
        boolean actualTrue = lList.add(index, number);

        //then
        assertTrue(actualTrue);
    }

    @org.junit.jupiter.api.Test
    void remove() {
        //given
        int expectedInt = 3;

        //when
        int actualInt = lList.remove(expectedInt);

        //then
        assertEquals(expectedInt, actualInt);
        assertNotEquals(2, actualInt);
    }

    @org.junit.jupiter.api.Test
    void removeByIndex() {
        //given
        int expectedInt = 1;
        int unexpected = 2;

        //when
        int actualInt = lList.removeByIndex(expectedInt);

        //then
        assertEquals(expectedInt, actualInt);
        assertNotEquals(unexpected, actualInt);
    }

    @org.junit.jupiter.api.Test
    void containsElement() {
        //given
        int numberOne = 3;

        //when
        boolean actualTrue = lList.contains(numberOne);

        //then
        assertTrue(actualTrue);
    }

    @org.junit.jupiter.api.Test
    void notContainsElement() {
        //given
        int numberTwo = 5;

        //when
        boolean actualFalse = lList.contains(numberTwo);

        //then
        assertFalse(actualFalse);
    }

    @org.junit.jupiter.api.Test
    void toArray() {
        //given

        //when
        int[] expected = {1, 2, 3};
        int[] actual = lList.toArray();

        //then
        assertEquals(Arrays.toString(expected), Arrays.toString(actual));
        assertNotEquals("{3}", Arrays.toString(actual));
    }

    @Test
    void subList() {
        //given
        int[] list = {1, 2, 3, 4, 5};
        LList lList = new LList(list);
        int fromIndex = 2;
        int toIndex = 3;

        //when
        int[] expected = {3, 4};
        IList newList = lList.subList(fromIndex, toIndex);
        int[] actual = newList.toArray();

        //then
        assertEquals(Arrays.toString(expected), Arrays.toString(actual));
        assertNotEquals(new int[]{1, 2}, actual);
    }

    @org.junit.jupiter.api.Test
    void removeAll() {
        //given

        //when
        boolean actual = lList.removeAll(lList.toArray());

        //then
        assertTrue(actual);
    }

    @org.junit.jupiter.api.Test
    void retainAll() {
        //given
        int[] array = {1};

        //when
        boolean actual = lList.retainAll(array);

        //then
        assertTrue(actual);
    }

    @org.junit.jupiter.api.Test
    void testToString() {
        //given

        //when
        String actual = lList.toString();
        String expected = "LList{head=Node{data=1, next=Node{data=2, next=Node{data=3, next=null}}}}";

        //then
        assertEquals(expected, actual);
        assertNotEquals("2", actual);
    }
}