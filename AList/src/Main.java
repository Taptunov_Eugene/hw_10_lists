import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        AList aList = new AList();
        aList.add(1);
        aList.add(2);
        System.out.println("Print AList: ");
        aList.print();
        aList.add(3);
        System.out.println("Print AList after add: ");
        aList.print();
        System.out.println("AList size is: " + aList.size() + "\n");
        aList.print();
        aList.add(3, 4);
        System.out.println("Print AList after add by index: ");
        aList.print();
        System.out.println("AList element by your index is " + aList.get(1) + "\n");
        System.out.println("AList contains provided element: " + aList.contains(2) + "\n");
        aList.remove(4); //удаляет индекс, а не число
        System.out.println("Print AList after removing: ");
        aList.print();
        aList.removeByIndex(1);
        System.out.println("Print AList after removing by index: ");
        aList.print();
        //aList.clear();
        aList.removeAll(aList.toArray());
        System.out.println("AList after remove all: ");
        aList.print();
    }
}
