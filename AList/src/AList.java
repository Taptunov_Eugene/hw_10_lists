import java.util.Arrays;

public class AList implements IList {

    private static final int DEFAULT_CAPACITY = 10;
    private int[] ints;
    private int size;

    public AList() {
        this.ints = new int[DEFAULT_CAPACITY];
    }

    public AList(int initialCapacity) {
        if (initialCapacity < 0) {
            throw new IllegalArgumentException("Illegal Capacity: " + initialCapacity);
        }
        this.ints = new int[initialCapacity];
    }

    public AList(int[] array) {
        if (array.length == 0) {
            ints = new int[array.length];
        } else {
            ints = new int[(int) (array.length * 1.2)];
        }
        System.arraycopy(array, 0, ints, 0, array.length);
    }

    @Override
    public void clear() {
        ints = new int[DEFAULT_CAPACITY];
        size = 0;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public int get(int index) {
        if (index > size - 1 || index < 0) {
            throw new ArrayIndexOutOfBoundsException("Specified index is out of range");
        }
        return ints[index];
    }

    private void ensureCapacity(int minCapacity) {
        int current = ints.length;
        if (minCapacity > current) {
            int[] newInts = new int[(int) (Math.ceil(ints.length + 1) * 1.2)];
            System.arraycopy(ints, 0, newInts, 0, size);
            ints = newInts;
        }
    }

    @Override
    public boolean add(int number) {
        if (size == ints.length) {
            ensureCapacity(size + 1);
        }
        ints[size++] = number;
        return true;
    }

    @Override
    public boolean add(int index, int number) {
        if (index > ints.length || index < 0) {
            throw new ArrayIndexOutOfBoundsException("Specified index is out of range");
        }
        if (index > size - 1 && index < ints.length) {
            add(number);
        }
        ensureCapacity(size + 1);
        ints[index] = number;
        //size++;
        return true;
    }

    @Override
    public int remove(int number) {

        for (int i = 0; i < ints.length; i++) {
            if (ints[i] == number) {
                removeByIndex(i);
            }
        }
        return number;
    }

    @Override
    public int removeByIndex(int index) {
        if (index > size || index < 0) {
            throw new ArrayIndexOutOfBoundsException("Specified index is out of range");
        }
        System.arraycopy(ints, index + 1, ints, index,
                ints.length - 1 - index);
        size--;
        return index;
    }

    @Override
    public boolean contains(int number) {
        for (int i = 0; i < size; i++) {
            if (ints[i] == number)
                return true;
        }

        return false;
    }

    @Override
    public void print() {
        System.out.println(Arrays.toString(toArray()));
    }

    @Override
    public int[] toArray() {
        int[] resultInts = new int[size];
        if (size >= 0) System.arraycopy(ints, 0, resultInts, 0, size);
        return resultInts;
    }

    @Override
    public IList subList(int fromIndex, int toIndex) {
        int j = 0;
        int[] resultInts = new int[(toIndex - fromIndex) + 1];
        for (int i = fromIndex; i < toIndex; i++) {
            resultInts[j++] = ints[i];
        }
        return new AList(resultInts);
    }

    @Override
    public boolean removeAll(int[] arr) {
        boolean result;
        for (int j : arr) {
            remove(j);
        }
        result = arr.length > 0;
        return result;
    }

    @Override
    public boolean retainAll(int[] arr) {
        int[] result = new int[ints.length];
        int k = 0;
        for (int anInt : ints) {
            for (int value : arr) {
                if (anInt == value) {
                    result[k++] = anInt;
                }
            }
        }
        ints = result;
        return true;
    }
}
